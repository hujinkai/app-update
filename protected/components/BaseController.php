<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BaseController extends Controller
{
	
	protected function renderJSON($data, $contentType = 'application/json')
    {
        $json = CJSON::encode($data);
        if (isset($_GET["jsonp"])) {
            header('Content-type: application/x-javascript');
            $json = sprintf("%s(%s)", $_GET["jsonp"], $json);
        } else {
            header("Content-type: $contentType");
        }
        echo $json;
        Yii::app()->end();
    }
}