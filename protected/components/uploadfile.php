<?php
class uploadfile {
    private $inputname;
    private $src;
    private $maxsize;
    private $allowtype;
    private $extension;

    function __construct($inputname, $src, $data=array()){

        $this->inputname = $inputname;
        $this->src = $src;
        isset($data['maxsize']) && $this->maxsize = $data['maxsize'];
        isset($data['allowtype']) && $this->allowtype = $data['allowtype'];
        isset($data['extension']) && $this->extension = $data['extension'];
    }

    function checkSize(){
        return $_FILES[$this->inputname]['size'] < $this->maxsize;
    }

    function checkType(){
        return in_array($_FILES[$this->inputname]['type'], $this->allowtype);
    }

    function checkExtension(){
        $pathInfo = pathinfo($_FILES[$this->inputname]['name']);
        return in_array($pathInfo['extension'], $this->extension);
    }

    function save(){
        if (!empty($this->allowtype) && !$this->checkType()) {
            return array('error' => '不允许的文件类型');
        }
        if (!empty($this->extension) && !$this->checkExtension()) {
            return array('error' => '不允许的文件后缀名');
        }
        if (!empty($this->maxsize) && !$this->checkSize()) {
            return array('error' => '超出允许上传的文件大小');
        }
        if(move_uploaded_file($_FILES[$this->inputname]['tmp_name'], $this->src)){
            return array('error'=>'');
        }else{
            return array('error'=>'上传失败！');
        }
    }
}
?>