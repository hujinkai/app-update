<?php
class AppController extends BaseController
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function actionIndex(){

        $data = Applib::model()->findAll();
        $items = array();
        foreach ($data as $app) {
            $item = $app->attributes;
            if($app->file){
                $url = 'http://'.$_SERVER['HTTP_HOST'].'/'.Yii::app()->params['config']['upload_relative'].'/'.$app->project->platform.'/';
                if($app->project->platform == 'android'){
                    $item['download_url'] = $url.$app->file->save_name.'.apk';
                }else{
                    $item['download_url'] = $url.$app->file->save_name.'.ipa';
                }
                $item['md5'] = $app->file->md5;
            }else{
                $item['download_url'] = '';
                $item['md5'] = '';
            }
            $item['project'] = $app->project->project;
            $item['platform'] = $app->project->platform;
            $items[] = $item;
        }
        $this->render('select',array("data"=>$items,'root'=>Yii::app()->params['config']['root']));
	}

    //添加版本
    public function actionAdd(){

        if(isset($_POST['submit'])&&$_POST['submit']==='提交'){

            $project = Project::model()->find('identifier=:identifier',array(':identifier'=>$_POST['identifier']));
            if($project){

                $now = date("Y-m-d H:i:s");

                $applib = new Applib;
                $applib->identifier = $_POST['identifier'];
                $applib->version = $_POST['version'];
                $applib->release_note = $_POST['release_note'];
                $applib->update_time = $now;
                $return = $applib->save();
                if($return){
                    $inputName = 'uploadfile';
                    
                    $config = Yii::app()->params['config'];
                    $prefix = $config['upload_absolute'];
                    $path = '/'.$project->platform.'/';

                    $pathInfo = pathinfo($_FILES[$inputName]['name']);
                    $md5Name = md5(uniqid(mt_rand(), true));
                    $filename = $md5Name . '.' . strtolower($pathInfo["extension"]);
                    $src = $prefix . $path . $filename;

                    if($project->platform == 'android'){
                        $file_limit = array(
                                'extension'=>array('apk')
                            );
                    }else{
                        $file_limit = array(
                                'extension'=>array('ipa')
                            );
                    }
                    $file = new uploadfile($inputName,$src,$file_limit);
                    $ret = $file->save();

                    if($ret['error']){
                        echo $ret['error'];
                    }else{
                        $file = new File;
                        $file->name = $_FILES[$inputName]['name'];
                        $file->save_name = $md5Name;
                        $file->type = $_FILES[$inputName]['type'];
                        $file->size = $_FILES[$inputName]['size'];
                        $file->md5 = md5_file($src);
                        $file->upload_time = $now;
                        $file->lib_id = $applib->id ;
                        $return = $file->save();
                        if($return){
                            if($project->platform == 'iphone'|| $project->platform == 'ipad'){

                                $ipa_dir = $prefix.$path;
                                $ipa_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$config['upload_relative'].$path;
                                $str = file_get_contents($ipa_dir.$project['project'].'.plist');

                                file_put_contents($prefix . $path . $md5Name . '.plist', str_replace('{ipa_url}',$ipa_url.$md5Name.'.ipa',$str)) ;
                            }
                            // echo '成功';
                            $this->redirect(array('app/')); 
                        }else{
                            echo '文件信息保存失败';
                        }
                    }
                }else{
                    echo '版本信保存失败';
                }
            }else{
                echo 'error identifier!';
            }
        }else{
            $this->render('add');
        }
    }

    // http://api/app/info?identifier=62ef9128e9fc7fa9a7bdef8bb5dbfc13  android
    // http://api/app/info?identifier=e16affe6856c04f6008cc9d0cdf28b18  iphone
    public function actionInfo(){
        $app = Applib::model()->find(array(
                'condition'=>'identifier=:identifier',
                'order'=>'sort desc,id desc',
                'params'=>array(':identifier'=>$_GET['identifier'])
            ));
        if($app){
            if($app->file){
                $url = 'http://'.$_SERVER['HTTP_HOST'].'/'.Yii::app()->params['config']['upload_relative'].'/'.$app->project->platform.'/';
                if($app->project->platform == 'android'){
                    $download_url = $url.$app->file->save_name.'.apk';
                }else{
                    $download_url = 'itms-services://?action=download-manifest&url='.$url.$app->file->save_name.'.plist';
                }
                $data = array(
                    'identifier'=>$app->identifier,
                    'version'=>$app->version,
                    'release_note'=>$app->release_note,
                    'download_url'=>$download_url,
                    'md5'=>$app->file->md5
                );
                $return = array('error_code'=>200, 'error_msg'=>'OK', 'data'=>$data);
            }else{
                $return = array('error_code'=>404, 'error_msg'=>'未找到文件');
            }
        }else{
            $return = array('error_code'=>404, 'error_msg'=>'');
        }
        $this->renderJSON($return);
    }

    public function actionDel(){

        $ret = Applib::model()->deleteByPk($_GET['id']);

        $this->redirect(array('app/')); 
        // $this->select('applib');
    }

    public function actionDelall(){
        foreach ($_POST['id'] as &$v) {
            $v = (int)$v;
        }
        $instr = implode(',', $_POST['id']);
        $ret = Applib::model()->deleteAll('id in ('.$instr.')');
        if($ret){
            //成功删除 $ret 条
            echo "OK";
        }else{
            echo "error";
        }
    }

    //更新数据
    public function actionUpd(){
        $data = Applib::model()->findByPk($_POST['id']);
        $data->version = $_POST['version'] ;
        $data->release_note = $_POST['release_note'] ;
        $data->sort = $_POST['sort'] ;
        $data->update_time = date("Y-m-d H:i:s");
        if($data->save()){
            $error_msg = "OK";
        }else{
            $error_msg = "XXX出错！";
        }
        echo $error_msg;
        //$this->select('applib');
    }

    public function actionEdit(){
        $data = Applib::model()->findByPk($_GET['id']);
        // $this->renderJSON($data);
        $this->renderPartial('edit',array('data'=>$data));
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','del','delAll','add','upd','edit'),
                'users'=>array('admin'),
            ),
            array('allow',
                'actions'=>array('index','add','upd','edit'),
                'users'=>array('upload'),
            ),
            array('deny',
                'actions'=>array('index','del','delAll','add','upd','edit'),
            ),
        );
    }

}