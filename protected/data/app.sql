-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: 127.0.0.1
-- 生成日期: 2013 年 11 月 08 日 10:15
-- 服务器版本: 5.5.32
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `app`
--
CREATE DATABASE IF NOT EXISTS `app-update` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `app-update`;

-- --------------------------------------------------------

--
-- 表的结构 `applib`
--

CREATE TABLE IF NOT EXISTS `applib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '项目级平台标识',
  `version` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '版本号',
  `release_note` varchar(500) COLLATE utf8_unicode_ci NOT NULL COMMENT '更新日志',
  `sort` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='版本库' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '上传文件名',
  `save_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT '保存文件名',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `md5` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `upload_time` datetime NOT NULL,
  `lib_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='上传文件' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '项目级平台标识',
  `platform` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '平台(全小写)',
  `project` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '项目名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='项目编码' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `project`
--

INSERT INTO `project` (`id`, `identifier`, `platform`, `project`) VALUES
(1, '62ef9128e9fc7fa9a7bdef8bb5dbfc13', 'android', 'liyuan'),
(2, 'e16affe6856c04f6008cc9d0cdf28b18', 'iphone', 'liyuan'),
(3, 'd9fb3593ec259cddcd4dbc7ce25528d7', 'ipad', 'liyuan'),
(4, '63482908ff0fa6e13b5437f71f1a56c2', 'android', 'yhc'),
(5, '7665d9ab5203de72b1cf995072c015db', 'ipad', 'yhc'),
(6, 'b76b5aadc8cd0b5a9682a0af7dd7b1a3', 'iphone', 'yhc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
