<?php

/**
 * This is the model class for table "applib".
 *
 * The followings are the available columns in table 'applib':
 * @property integer $id
 * @property string $identifier
 * @property string $version
 * @property string $release_note
 * @property integer $sort
 */
class Applib extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Applib the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'applib';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('identifier, version, release_note', 'required'),
			array('sort', 'numerical', 'integerOnly'=>true),
			array('identifier', 'length', 'max'=>32),
			array('version', 'length', 'max'=>10),
			array('release_note', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, identifier, version, release_note, sort', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'file' => array(self::HAS_ONE, 'File', 'lib_id'),
			'project' => array(self::BELONGS_TO, 'Project', array('identifier'=>'identifier')),
		);
	}
	
	public function defaultScope()
    {
        return array(
            'order'=>'id DESC',
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'identifier' => 'Identifier',
			'version' => 'Version',
			'release_note' => 'Release Note',
			'sort' => 'Sort',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('identifier',$this->identifier,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('release_note',$this->release_note,true);
		$criteria->compare('sort',$this->sort);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}