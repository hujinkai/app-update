<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property string $name
 * @property string $save_name
 * @property string $type
 * @property integer $size
 * @property string $md5
 * @property string $upload_time
 * @property string $lib_id
 */
class File extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, save_name, type, size, md5, upload_time', 'required'),
			array('size, lib_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('save_name', 'length', 'max'=>40),
			array('type', 'length', 'max'=>50),
			array('md5', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, save_name, type, size, md5, upload_time, lib_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'save_name' => 'Save Name',
			'type' => 'Type',
			'size' => 'Size',
			'md5' => 'Md5',
			'upload_time' => 'Upload Time',
			'lib_id' => 'lib_id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('save_name',$this->save_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('md5',$this->md5,true);
		$criteria->compare('upload_time',$this->upload_time,true);
		$criteria->compare('lib_id',$this->lib_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}