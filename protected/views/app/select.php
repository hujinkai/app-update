<script type="text/javascript">

	function load(obj){
		
		var dialogId = 'mymodal';
		$.ajax({
            'type': 'POST',
            'url' : $(obj).attr('href'),
            success: function (data) {
                $('#'+dialogId+' div.divForForm').html(data);
                $( '#'+dialogId ).dialog( 'open' );
            },
            dataType: 'html'
        });
	}

	function edit(){
		$.post("<?php echo $this->createUrl('app/upd') ?>", $("#edit").serialize(),function(data){
			if(data == 'OK'){
				$( '#mymodal' ).dialog( 'close' );
				location=location;
				alert('保存成功!');
			}
    	});
	}

	function pagelist(id){
		$('#'+id+' .checkall').click(function(){
			$('#'+id+' .idcheck').each(function(){
				this.checked = true;
			});
		});
		$('#'+id+' .checkother').click(function(){
			$('#'+id+' .idcheck').each(function(){
				this.checked = !this.checked;
			});
		});
		$('#'+id+' .delchecked').click(function(){
			if(confirm("确定要删除选中的记录?")){
				$.post("<?php echo $this->createUrl('app/delall') ?>", $("#"+id+" form").serialize(),function(data){
					if(data == 'OK'){
						location=location;
						alert("删除成功！");
					}
		    	});
			}
		});
	}

	$(function(){

		pagelist('page');

	});
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mymodal',
    'options'=>array(
                'title'=>'编辑',
                'width'=>500,
                // 'height'=>500,
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'overlay'=>array(
                    'backgroundColor'=>'#000',
                    'opacity'=>'0.5'
                    ),
                'buttons'=>array(
                    '保存'=>'js:function(){ edit();}',
                    '取消'=>'js:function(){$(this).dialog("close");}',
                    ),
                ),
            ));
echo '<div class="divForForm">loading....</div>';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<div id="page" class="pagelist">
	<div>
		<span>[<a href="javascript:void(0);" class="checkall">全选</a>] / [<a href="javascript:void(0);" class="checkother">反选</a>]</span>
		<span>　</span>
		<span>[<a href="javascript:void(0);" class="delchecked">批量删除</a>]</span>
		<span style="float:right;"><a href="<?php echo $this->createUrl('app/add') ?>">添加</a></span>
	</div>
	<br>
	<form>
		<table>
			<tr>
				<td>选择</td>
				<td>id</td>
				<td>标识</td>
				<td>项目</td>
				<td>平台</td>
				<td>版本</td>
				<td>说明</td>
				<td>优先</td>
				<td>更新时间</td>
				<td>文件</td>
				<td>操作</td>
			</tr>
			<?php foreach ($data as $v) {

				echo '<tr>';
				echo '<td><input type="checkbox" class="idcheck" name="id[]" value="',$v['id'],'"></td>';
				echo '<td>'.$v['id'].'</td>';
				echo '<td><a href="',$root,'/app/info?identifier=',$v['identifier'],'">',$v['identifier'],'</a></td>';
				echo '<td>'.$v['project'].'</td>';
				echo '<td>'.$v['platform'].'</td>';
				echo '<td>'.$v['version'].'</td>';
				// echo '<td>'.$v['download_url'].'</td>';
				echo '<td>'.$v['release_note'].'</td>';
				echo '<td>'.$v['sort'].'</td>';
				echo '<td>'.$v['update_time'].'</td>';
				echo '<td><a href="',$v['download_url'],'">下载</a> <a href="#" title="',$v['md5'],'">md5</a></td>';
				echo '<td>',CHtml::link('编辑',$this->createUrl('app/edit',array('id'=>$v['id'])),array('onclick'=>'load(this); return false;')),
					' <a href="',$this->createUrl('app/del',array('id'=>$v['id'])),
					'" onclick="return confirm(\'确认删除?\');" >删除</a></td>';
				echo "</tr>";

			}?>
		</table>
	</form>
</div>
